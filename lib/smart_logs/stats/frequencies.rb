module SmartLogs
  module Stats
    class Frequencies
      include Concerns::Presentable
      include Concerns::Orderable

      def initialize(log_lines = [])
        @log_lines = log_lines
      end

      def compute
        return [] if log_lines.empty?

        order(log_lines.group_by(&:path).transform_values!(&:size))
      end

      private

      attr_reader :log_lines
    end
  end
end
