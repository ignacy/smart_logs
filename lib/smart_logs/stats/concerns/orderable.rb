module SmartLogs
  module Stats
    module Concerns
      module Orderable
        # All statistics are reduced to the following format:
        # [
        #   ["path", statistic_value]
        #   ...
        # ]
        # We want them to be ordered from the largest so we sort by the second field
        # and we use `-` so we don't have to reverse the collection later
        def order(stats)
          stats.sort_by { |stat| -stat[1] }
        end
      end
    end
  end
end
