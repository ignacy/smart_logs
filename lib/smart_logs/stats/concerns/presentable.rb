module SmartLogs
  module Stats
    module Concerns
      module Presentable
        def presenter
          stat_class_name = self.class.name.split('::').last
          Kernel.const_get("#{::SmartLogs::Presenters}::#{stat_class_name}Presenter")
        end

        def print
          presenter.new(compute).show
        end
      end
    end
  end
end
