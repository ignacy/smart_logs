module SmartLogs
  module Stats
    class UniqueVisitors
      include Concerns::Presentable
      include Concerns::Orderable

      def initialize(log_lines = [])
        @log_lines = log_lines
      end

      def compute
        return [] if log_lines.empty?

        unique_per_path = log_lines.group_by(&:path).map do |path, lines|
          [path, lines.map(&:source_ip).uniq.count]
        end

        order(unique_per_path)
      end

      private

      attr_reader :log_lines
    end
  end
end
