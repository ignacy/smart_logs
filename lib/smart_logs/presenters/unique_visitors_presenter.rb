module SmartLogs
  module Presenters
    class UniqueVisitorsPresenter < BasePresenter
      def caption
        'unique visitors'
      end

      def line_caption
        'unique views'
      end
    end
  end
end
