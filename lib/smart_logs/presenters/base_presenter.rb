module SmartLogs
  module Presenters
    class BasePresenter
      def initialize(stats_collection)
        @stats_collection = stats_collection
      end

      def show
        unless stats_collection.any?
          puts "No statistics for: #{caption}"
          return
        end

        puts "Showing statistics for: #{caption}"

        stats_collection.each do |stat|
          puts "#{stat.first.ljust(20)} #{stat.last} #{line_caption}"
        end
      end

      def caption
        raise NotImplementedError
      end

      def line_caption
        raise NotImplementedError
      end

      private

      attr_reader :stats_collection
    end
  end
end
