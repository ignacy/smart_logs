module SmartLogs
  module Presenters
    class FrequenciesPresenter < BasePresenter
      def caption
        'most views'
      end

      def line_caption
        'views'
      end
    end
  end
end
