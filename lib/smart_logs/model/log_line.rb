module SmartLogs
  module Model
    class LogLine
      def initialize(raw_line)
        parts = raw_line.split
        @path = parts[0]
        @source_ip = parts[1]
      end

      attr_reader :path, :source_ip

      def valid?
        !path.nil? && !source_ip.nil?
      end
    end
  end
end
