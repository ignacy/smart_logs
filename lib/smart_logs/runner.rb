module SmartLogs
  class Runner
    def initialize(path, statistics = ::SmartLogs.available_statistics)
      @log_lines = ::SmartLogs::Parser.new(path).call
      @statistics = statistics
    end

    def call
      statistics.each do |stats_class|
        stats_class.new(log_lines).print
      end
    end

    private

    attr_reader :log_lines, :statistics
  end
end
