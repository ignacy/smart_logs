module SmartLogs
  class Parser
    def initialize(path)
      raise ArgumentError, 'path cannot be empty' if path.nil? || path == ''

      @path = path
    end

    def call
      raise ArgumentError, 'log file seems to be empty' if raw_lines.empty?

      raw_lines.each_with_object([]) do |raw_line, log_lines|
        log_line = ::SmartLogs::Model::LogLine.new(raw_line)
        log_lines << log_line if log_line.valid?
      end
    end

    private

    def raw_lines
      @raw_lines ||= begin
        File.readlines(path).map(&:chomp)
      rescue StandardError
        []
      end
    end

    attr_reader :path
  end
end
