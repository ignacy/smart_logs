# frozen_string_literal: true

require 'zeitwerk'
loader = Zeitwerk::Loader.for_gem
loader.setup

module SmartLogs
  def self.available_statistics
    ::SmartLogs::Stats
      .constants
      .select { |c| ::SmartLogs::Stats.const_get(c).is_a? Class }
      .map { |c| Kernel.const_get("::SmartLogs::Stats::#{c}") }
  end
end

loader.eager_load
