# Usage

To run the code execute:

```sh

bin/parser webserver.log

```

![example output](resources/run.png)

# Design choices

Obviously, this is just one of many way of approaching this problem. I'd like to mention two decisions I made:

* The main entry point `::SmartLogs::Runner` does not know which types of statistics are available and how they are presented.

This means it follows the [Open/Closed Principle](https://www.wikiwand.com/en/Open%E2%80%93closed_principle). In practical sense this means that adding new statistics is as simple as introducing new class in `::SmartLogs::Stats` and a new presenter for it in `::SmartLogs::Presenters`.

One downside of this approach is that we use a light form of reflection to find Stats and Presenter classes, but most of that would be handled by external libraries in larger projects.

* I take the `best effort` approach to interpreting the log files

There are some checks in place for empty/missing files or for log lines that are missing one of the fields. It would be possible to filter the log lines not matching the expected "path ip" format. For example, code like this could be used to validate log parts:

``` ruby
path = URI(parts[0]).path
source_ip = IPAddr.new(parts[1]).to_s
```

I decided against this because it would make the srcipt much slower on larger files and it seems plausible to assume log file structure will be in the expected format. 

# Automatic checks for code quality

The code uses rubocop, rspec and rcov which all can be run using the `check` command. Current test coverage is at ~98%:

``` sh
> bin/check

SmartLogs::Model::LogLine
  validation
    is valid when both fields are present
    is invalid when both fields are missing
    is invalid when path field is missing
    is invalid when address field is missing
    is invalid when there are superflous fields given

SmartLogs::Parser
  returns a list of parsed log entries
    correctly parses simple input, and returns a list of LogLines
    ignores malformed log lines from the output
  wrong input file
    fails when no file path is passed on as parameter
    fails when file points to unreadable file

SmartLogs::Presenters::FrequenciesPresenter
  computes no frequencies for empty input collection
  computes frequency stats correctly for LogLine collection

SmartLogs::Runner
  computes and presents all available statistics
  can compute only a subset of statistics

SmartLogs::Stats::Frequencies
  computes no frequencies for empty input collection
  computes frequency stats correctly for LogLine collection

SmartLogs::Stats::UniqueVisitors
  computes no unique visitors for empty input collection
  computes unique visitors stats correctly for LogLine collection

Finished in 0.00977 seconds (files took 0.18937 seconds to load)
17 examples, 0 failures

Coverage report generated for RSpec to /Users/ignacy/code/smart_logs/coverage. 170 / 173 LOC (98.27%) covered.
Inspecting 19 files
...................

```

# Coverage report:

![coverage](resources/coverage.png)
