# frozen_string_literal: true

RSpec.describe SmartLogs::Parser do
  context 'returns a list of parsed log entries ' do
    it 'correctly parses simple input, and returns a list of LogLines' do
      lines = described_class.new('spec/fixtures/simple_stats.log').call
      expect(lines).to_not be_empty

      line = lines.first

      expect(line.path).to eq('/home')
      expect(line.source_ip).to eq('184.123.665.067')
    end

    it 'ignores malformed log lines from the output ' do
      lines = described_class.new('spec/fixtures/malformed_line.log').call
      expect(lines).to_not be_empty

      line = lines.first

      expect(line.path).to eq('/contact')
      expect(line.source_ip).to eq('1.2.3.4')
    end
  end

  context 'wrong input file' do
    it 'fails when no file path is passed on as parameter' do
      expect do
        described_class.new.call
      end.to raise_error(ArgumentError)
    end

    it 'fails when file points to unreadable file' do
      expect do
        described_class.new('spec/fixtures/empty.log').call
      end.to raise_error(ArgumentError)
    end
  end
end
