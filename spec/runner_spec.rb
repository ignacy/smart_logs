# frozen_string_literal: true

RSpec.describe SmartLogs::Runner do
  it 'computes and presents all available statistics' do
    expect do
      described_class.new('spec/fixtures/simple_stats.log').call
    end.to output(
      <<~OUTPUT
        Showing statistics for: most views
        /help_page/1         4 views
        /home                3 views
        Showing statistics for: unique visitors
        /home                3 unique views
        /help_page/1         1 unique views
      OUTPUT
    ).to_stdout
  end

  it 'can compute only a subset of statistics' do
    expect do
      described_class.new('spec/fixtures/simple_stats.log', [::SmartLogs::Stats::UniqueVisitors]).call
    end.to output(
      <<~OUTPUT
        Showing statistics for: unique visitors
        /home                3 unique views
        /help_page/1         1 unique views
      OUTPUT
    ).to_stdout
  end
end
