# frozen_string_literal: true

RSpec.describe SmartLogs::Model::LogLine do
  context 'validation' do
    it 'is valid when both fields are present' do
      expect(described_class.new('/home 1.2.3.4')).to be_valid
    end

    it 'is invalid when both fields are missing' do
      expect(described_class.new('')).to_not be_valid
    end

    it 'is invalid when path field is missing' do
      expect(described_class.new('1.2.3.4')).to_not be_valid
    end

    it 'is invalid when address field is missing' do
      expect(described_class.new('/home')).to_not be_valid
    end

    it 'is valid when there are superflous fields given' do
      # This assumption might be debatable but I choose to
      # consider a raw line to be valid with even if there is more data
      # than what we need
      expect(described_class.new('/home 1.2.3.4 a b c')).to be_valid
    end
  end
end
