# frozen_string_literal: true

RSpec.describe SmartLogs::Presenters::FrequenciesPresenter do
  it 'computes no frequencies for empty input collection' do
    expect { described_class.new([]).show }
      .to output("No statistics for: most views\n")
      .to_stdout
  end

  it 'computes frequency stats correctly for LogLine collection' do
    lines = [
      SmartLogs::Model::LogLine.new('/home 1.2.3.4'),
      SmartLogs::Model::LogLine.new('/home 4.3.2.1'),
      SmartLogs::Model::LogLine.new('/contact 222.2.3.4')
    ]

    stats = ::SmartLogs::Stats::Frequencies.new(lines).compute

    expect { described_class.new(stats).show }
      .to output("Showing statistics for: most views\n/home                2 views\n/contact             1 views\n")
      .to_stdout
  end
end
