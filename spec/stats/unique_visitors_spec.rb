# frozen_string_literal: true

RSpec.describe SmartLogs::Stats::UniqueVisitors do
  it 'computes no unique visitors for empty input collection' do
    expect(described_class.new([]).compute).to eq([])
  end

  it 'computes unique visitors stats correctly for LogLine collection' do
    lines = [
      SmartLogs::Model::LogLine.new('/home 1.2.3.4'),
      SmartLogs::Model::LogLine.new('/home 4.3.2.1'),
      SmartLogs::Model::LogLine.new('/contact 222.2.3.4'),
      SmartLogs::Model::LogLine.new('/contact 222.2.3.4')
    ]

    expect(described_class.new(lines).compute).to match_array(
      [
        ['/home', 2],
        ['/contact', 1]
      ]
    )
  end
end
